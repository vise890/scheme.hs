module Main where

import           Evaluator
import           Parser

import           System.Environment

main :: IO ()
main = getArgs >>= print . eval . readExpr . head
