module EvaluatorSpec where

import           Test.Hspec

import           Evaluator
import           LispVal

spec :: Spec
spec = do

  -- FIXME: these should all be QuickCheck tests
  describe "evaluating Primitive LispVals" $ do

    it "works for Strings" $
      eval (String "foo") `shouldBe` String "foo"

    it "works for Characters" $
      eval (Character 'a') `shouldBe` Character 'a'

    it "works for Booleans" $
      eval (Boolean True) `shouldBe` Boolean True

    it "works for Numbers" $
      eval (Number 1) `shouldBe` Number 1

    it "works for Floats" $
      eval (Float 4.2) `shouldBe` Float 4.2

    it "works for quoted LispVals" $ do
      eval (List [Atom "quote", Number 1]) `shouldBe` Number 1
      eval (List [Atom "quote", String "42"]) `shouldBe` String "42"

  describe "evaluating Lists with primitive procedures" $ do
    -- TODO: should probably test / * ^ etc.

    it "works for addition on Integers" $ do
      eval (List [Atom "+", Number 1, Number 2]) `shouldBe` Number 3
      eval (List [Atom "+", Number 1, Number 1, Number 40]) `shouldBe` Number 42

    it "works for subtraction on Integers" $
      eval (List [Atom "-", Number 5, Number 2]) `shouldBe` Number 3

    it "works for nested lists" $
      eval (List [ Atom "-"
                 , List [ Atom "+"
                        , Number 4
                        , Number 6
                        , Number 3]
                 , Number 3
                 , Number 5
                 , Number 2]) `shouldBe` Number 3

    it "works for other operations" $ do
      eval (List [Atom "*", Number 3, Number 5]) `shouldBe` Number 15
      eval (List [Atom "/", Number 10, Number 7]) `shouldBe` Number 1
      eval (List [Atom "mod", Number 10, Number 7]) `shouldBe` Number 3
      -- FIXME: etc. these should all be migrated to QuickCheck
