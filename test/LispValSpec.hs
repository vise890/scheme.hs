module LispValSpec where

import           Test.Hspec

import           LispVal

spec :: Spec
spec =

  describe "showing a LispVal" $

    it "produces a string representation for the underlying value" $
      shown `shouldBe` "(quote (1 4.2 #t . #f) #\\a)"
      where shown = show $ List [ Atom "quote"
                                , DottedList [ Number 1
                                              , Float 4.2
                                              , Boolean True
                                              ]
                                              (Boolean False)
                                , Character 'a'
                                ]
