module ParserSpec where

import           Test.Hspec
import           Text.ParserCombinators.Parsec as Parsec

import           LispVal
import           Parser

parse' :: String -> LispVal
parse' input = case Parsec.parse parseExpr "tester" input of
                 Left err  -> error $ "No parse: " ++ show err
                 Right val -> val

spec :: Spec
spec = do

  describe "parsing Booleans" $ do

    it "works for True" $
      parse' "#t" `shouldBe` Boolean True

    it "works for False" $
      parse' "#f" `shouldBe` Boolean False

  describe "parsing Atoms" $

    it "returns a correctly named Atom" $ do
      parse' "atomus" `shouldBe` Atom "atomus"
      parse' "_atomus" `shouldBe` Atom "_atomus"

  describe "parsing Strings" $ do

    it "works for a simple string" $
      parse' "\"foo\"" `shouldBe` String "foo"

    it "successfully deals with escape characters" $ do
      parse' "\" \\\" \"" `shouldBe` String " \" "
      parse' "\" \\\\ \"" `shouldBe` String " \\ "
      parse' "\" \\r \"" `shouldBe` String " \r "
      parse' "\" \\t \"" `shouldBe` String " \t "
      parse' "\" \\n \"" `shouldBe` String " \n "

  describe "parsing Characters" $ do

    it "returns the correct Character" $
      parse' "#\\a" `shouldBe` Character 'a'

    it "returns the corresponding Character when using the character name syntax" $ do
      parse' "#\\space" `shouldBe` Character ' '
      parse' "#\\newline" `shouldBe` Character '\n'

  describe "parsing Numbers" $ do

    it "treats numbers without a radix prefix as decimals" $
      parse' "42" `shouldBe` Number 42

    it "treats numbers with radix prefix `#d` as decimals" $
      parse' "#d42" `shouldBe` Number 42

    it "treats numbers with radix prefix `#o` as octals" $ do
      parse' "#o52" `shouldBe` Number 42
      parse' "#o173" `shouldBe` Number 123

    it "treats numbers with radix prefix `#x` as hexadecimals" $ do
        parse' "#x2A" `shouldBe` Number 42
        parse' "#x7B" `shouldBe` Number 123

  describe "parsing Floats" $ do

    it "returns the correct Float" $
      parse' "1.2" `shouldBe` Float 1.2

    it "works for my very own, fully R5RS non-compliant notation" $
      parse' "1." `shouldBe` Float 1.0

  describe "parsing Lists" $ do

    it "works for a simple List" $
      parse' "(+ 1 2 3)" `shouldBe` List [Atom "+", Number 1, Number 2, Number 3]

    it "works for a nested List" $
      parse' "(foo #t 1.2 (+ 1 2))" `shouldBe` List [ Atom "foo"
                                                    , Boolean True
                                                    , Float 1.2
                                                    , List [ Atom "+"
                                                           , Number 1
                                                           , Number 2
                                                           ]
                                                    ]

  describe "parsing DottedLists" $ do

    it "works for a simple DottedList" $
      parse' "(1 2 . 3)" `shouldBe` DottedList [Number 1, Number 2]
                                               (Number 3)

    it "works for a nested DottedList" $
      parse' "(1 2 . (1 2 . 3))" `shouldBe` DottedList [Number 1, Number 2]
                                                       (DottedList [Number 1, Number 2]
                                                                   (Number 3))

  describe "parsing quoted expressions" $ do

    it "works for a quoted Atom" $
      parse'  "'atom" `shouldBe` List [Atom "quote", Atom "atom"]

    it "works for a quoted List" $
      parse' "'(1 2 3)" `shouldBe` List [ Atom "quote"
                                        , List [Number 1, Number 2, Number 3]
                                        ]
