# Write myself a scheme in 48(+) hours

Based on the [tutorial found on
wikibooks](https://en.wikibooks.org/wiki/Write_Yourself_a_Scheme_in_48_Hours)

# Running
```bash
$ cabal run "(+ 1 2 3)"
```

# Running tests
```bash
$ stack test
```
