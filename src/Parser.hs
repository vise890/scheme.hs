module Parser
( readExpr
, parseExpr
) where

import           Numeric
import           Text.ParserCombinators.Parsec

import           LispVal

readExpr :: String -> LispVal
readExpr input = case parse parseExpr "lisp" input of
                   Left err  -> String $ "No parse: " ++ show err
                   Right val -> val

parseExpr :: Parser LispVal
parseExpr = parseString
        <|> try parseChar -- NOTE: relies on parser order to diff char / bool
        <|> try parseBoolean
        <|> parseAtom
        <|> try parseFloat  -- NOTE: relies on parser order to diff between Floats / Numbers
        <|> try parseNumber
        <|> parseList
        <|> parseQuoted

{-# ANN parseString "HLint: ignore Use string literal" #-}
parseString :: Parser LispVal
parseString = do char '"'
                 s <- many $ escapedChar <|> noneOf ['\"', '\\']
                 char '"'
                 return $ String s

parseChar :: Parser LispVal
parseChar = do string "#\\"
               parseCharName <|> parseChar'
               where parseChar' = do c <- anyChar
                                     return $ Character c
                     parseCharName = do cn <- string "space" <|> string "newline"
                                        return $ case cn of
                                                   "space"   -> Character ' '
                                                   "newline" -> Character '\n'
                                                   _ -> error "impossible"

parseBoolean :: Parser LispVal
parseBoolean = do char '#'
                  b <- oneOf "tf"
                  return $ case b of
                             't' -> Boolean True
                             'f' -> Boolean False
                             _   -> error "impossible"

-- an atom is a letter or a symbol, followed by any number of letters, sybmols
-- or digits
parseAtom :: Parser LispVal
parseAtom = do first <- letter <|> symbol
               rest <- many (letter <|> symbol <|> digit)
               return $ Atom (first:rest)

parseNumber :: Parser LispVal
parseNumber = parseRawNumber
          <|> parseNumberWithRadix

-- if a number doesn't have a radix prefix
-- it is assumed to be expressed in decimal
parseRawNumber :: Parser LispVal
parseRawNumber = do n <- many1 digit
                    return $ Number (read n)

parseNumberWithRadix :: Parser LispVal
parseNumberWithRadix = do char '#'
                          r <- oneOf "dox"
                          case r of
                            'd' -> parseRawNumber
                            'o' -> parseOctal
                            'x' -> parseHex
                            _   -> error "impossible"

parseOctal :: Parser LispVal
parseOctal = do o <- many1 octDigit
                return $ Number (readOct' o)
                where readOct' = fst . head . readOct

parseHex :: Parser LispVal
parseHex = do x <- many1 hexDigit
              return $ Number (readHex' x)
              where readHex' = fst . head . readHex

parseFloat :: Parser LispVal
parseFloat =  do int <- many1 digit
                 char '.'
                 dec <- many digit
                 return $ Float (readFloat' $ int ++ "." ++ dec)
                 where readFloat' = fst . head . readFloat

-- parse a list: either a Proper List or a DottedList (improper list)
parseList :: Parser LispVal
parseList = do char '('
               xs <- parseProperList
               x <- optionMaybe parseDottedListEnd
               char ')'
               return $ case x of
                 Nothing  -> List xs
                 Just val -> DottedList xs val

parseProperList :: Parser [LispVal]
parseProperList = parseExpr `sepEndBy` spaces1

parseDottedListEnd :: Parser LispVal
parseDottedListEnd = char '.' >> spaces1 >> parseExpr

parseQuoted :: Parser LispVal
parseQuoted = do char '\''
                 x <- parseExpr
                 return $ List [Atom "quote", x]

{-# ANN escapedChar "HLint: ignore Use string literal" #-}
escapedChar :: Parser Char
escapedChar = do char '\\'
                 c <- oneOf ['\"', '\\', 'r', 't', 'n']
                 return $ case c of
                           't' -> '\t'
                           'n' -> '\n'
                           'r' -> '\r'
                           _   -> c

symbol :: Parser Char
symbol = oneOf "!$%&|*+-/:<=?>@^_~"

spaces1 :: Parser String
spaces1 = many1 space
