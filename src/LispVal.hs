module LispVal (LispVal(..)) where

data LispVal = String String
             | Character Char
             | Boolean Bool
             | Number Integer
             | Float Float
             -- An atom, with a name
             | Atom String
             -- A proper linked list
             | List [LispVal]
             -- An "improper" list with its last el in a separate field
             | DottedList [LispVal] LispVal
             deriving (Eq)

instance Show LispVal where
  show val =
    case val of
      (String s)        -> "\"" ++ s ++ "\""
      (Character c)     -> "#\\" ++ [c]
      (Boolean b)       -> if b then "#t" else "#f"
      (Number n)        -> show n
      (Float f)         -> show f
      (Atom name)       -> name
      (List vs)         -> "(" ++ showList' vs ++ ")"
      (DottedList vs v) -> "(" ++ showList' vs ++ " . " ++ show v ++ ")"
      where showList' = unwords . map show
