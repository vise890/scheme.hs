module Evaluator (eval) where

import           LispVal

eval :: LispVal -> LispVal
-- Primitive values evaluate to themselves:
eval v@(String _)             = v
eval v@(Character _)          = v
eval v@(Boolean _)            = v
eval v@(Number _)             = v
eval v@(Float _)              = v
eval (List [Atom "quote", v]) = v
eval (List (Atom funcName:args)) = apply funcName $ map eval args
eval x                        = error $ "Evaluation of " ++ show x ++ " is not implemented yet"

apply :: String -> [LispVal] -> LispVal
apply funcName args = case func of
                        Just func' -> func' args
                        Nothing    -> error $ "Could not find an implementation for function named: " ++ funcName
                      where func = lookup funcName primitives

primitives :: [(String, [LispVal] -> LispVal)]
primitives = [ ("+", numericBinOp (+))
             , ("-", numericBinOp (-))
             , ("*", numericBinOp (*))
             , ("/", numericBinOp div)
             , ("mod", numericBinOp mod)
             , ("quotient", numericBinOp quot)
             , ("remainder", numericBinOp rem)]

numericBinOp :: (Integer -> Integer -> Integer) -> [LispVal] -> LispVal
numericBinOp op args = Number $ foldl1 op $ map unpackNumber args

-- FIXME: this should work for Float LispVals too
unpackNumber :: LispVal -> Integer
unpackNumber (Number i) = i :: Integer
unpackNumber v          = error $ "Cannot convert " ++ show v ++ "into a Num"
